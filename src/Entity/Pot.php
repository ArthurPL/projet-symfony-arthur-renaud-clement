<?php

namespace App\Entity;

use App\Repository\PotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PotRepository::class)
 */
class Pot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantTot;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantMoy;

    /**
     * @ORM\OneToMany(targetEntity=Person::class, mappedBy="idPot", orphanRemoval=true)
     */
    private $idPot;

    public function __construct()
    {
        $this->idPot = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMontantTot(): ?float
    {
        return $this->montantTot;
    }

    public function setMontantTot(?float $montantTot): self
    {
        $this->montantTot = $montantTot;

        return $this;
    }

    public function getMontantMoy(): ?float
    {
        return $this->montantMoy;
    }

    public function setMontantMoy(?float $montantMoy): self
    {
        $this->montantMoy = $montantMoy;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getIdPot(): Collection
    {
        return $this->idPot;
    }

    public function addIdPot(Person $idPot): self
    {
        if (!$this->idPot->contains($idPot)) {
            $this->idPot[] = $idPot;
            $idPot->setIdPot($this);
        }

        return $this;
    }

    public function removeIdPot(Person $idPot): self
    {
        if ($this->idPot->removeElement($idPot)) {
            // set the owning side to null (unless already changed)
            if ($idPot->getIdPot() === $this) {
                $idPot->setIdPot(null);
            }
        }

        return $this;
    }
}
