<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201111121801 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176A14E0760');
        $this->addSql('DROP INDEX IDX_34DCD176A14E0760 ON person');
        $this->addSql('ALTER TABLE person DROP id_person_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE person ADD id_person_id INT NOT NULL');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A14E0760 FOREIGN KEY (id_person_id) REFERENCES pot (id)');
        $this->addSql('CREATE INDEX IDX_34DCD176A14E0760 ON person (id_person_id)');
    }
}
