<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $montantPaye;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantRestant;

    /**
     * @ORM\ManyToOne(targetEntity=Pot::class, inversedBy="idPot")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idPot;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMontantPaye(): ?float
    {
        return $this->montantPaye;
    }

    public function setMontantPaye(float $montantPaye): self
    {
        $this->montantPaye = $montantPaye;

        return $this;
    }

    public function getMontantRestant(): ?float
    {
        return $this->montantRestant;
    }

    public function setMontantRestant(?float $montantRestant): self
    {
        $this->montantRestant = $montantRestant;

        return $this;
    }

    public function getIdPerson(): ?Pot
    {
        return $this->idPerson;
    }

    public function setIdPerson(?Pot $idPerson): self
    {
        $this->idPerson = $idPerson;

        return $this;
    }

    public function getIdPot(): ?Pot
    {
        return $this->idPot;
    }

    public function setIdPot(?Pot $idPot): self
    {
        $this->idPot = $idPot;

        return $this;
    }
}
