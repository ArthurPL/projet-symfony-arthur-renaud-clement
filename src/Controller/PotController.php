<?php

namespace App\Controller;

use App\Entity\Pot;
use App\Form\PotType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PotController extends AbstractController
{
    /**
     * @Route("/pot", name="pot")
     */
    public function index()
    {
        //récupérer le repository
        $repository = $this->getDoctrine()->getRepository(Pot::class);
        //je lis la BDD
        $pot = $repository->findAll();

        $nameLastPot = $this->lastPotCreated();
        $persons = $this->getPerson();
        return $this->render('/pot/index.html.twig', [
            'controller_name' => 'Arthur',
            "pot" => $pot ,
            'lastPot' => $nameLastPot,
            'persons' => $persons,
        ]);
    }

    /**
     * @Route("/ajouterPot", name="potAjouter")
     */
    public function ajouter(Request $request){

        //créer un pot vide
        $pot = new Pot();

        //créer le formulaire
        $form = $this->createForm(PotType::class, $pot);

        //gérer le retour du POST
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //récupérer l'entity manager (objet qui gère la connection à la BDD)
            $em = $this->getDoctrine()->getManager();

            //je dis au manager que je veux garde l'objet en BDD
            $em->persist($pot);

            //je déclenche l'insert
            $em->flush();


            //je vais à la liste des soirees
            return $this->redirectToRoute('pot');

        }
        return $this->render("/pot/ajouter.html.twig", [
            "formulaire" => $form->createView()]);
    }



    public function lastPotCreated(){
        $em=$this->getDoctrine()->getManager();

        $query='SELECT name, id, montant_tot FROM pot ORDER BY id DESC LIMIT 0, 1';

        $res = $em->getConnection()->prepare($query);

        $res->execute();

        $reponse = $res->fetchAll();
        return $reponse;
    }

    public function getPerson(){
        $nameLastPot = $this->lastPotCreated();

        $id = $nameLastPot[0]['id'];
        //$id = $this->lastPotCreated()['id']->getId
        $em= $this->getDoctrine()->getManager();
        $query = 'SELECT name, id, montant_paye, montant_restant FROM person where id_pot_id =:idpot';
        $req = $em->getConnection()->prepare($query);
        $req->execute(['idpot' => $id]);
        $reponse = $req->fetchAll();

        return $reponse;

    }

    /**
     * @Route("final", name="final")
     */
    public function final(Request $request){
        $pot = $this->lastPotCreated();
        $entityManager = $this->getDoctrine()->getManager();
        $pot = $entityManager->getRepository(Pot::class)->find($pot[0]['id']);




        $moy = $this->montantMoy();
        $tot = $pot->getMontantTot();
        $persons = $this->getPerson();
        //$tot = $this->verifMontantTot();

        return $this->render("/pot/final.html.twig", [
            'total' => $tot,
            'persons' => $persons,
            'moy'=> $moy,
        ]);
    }

    public function calculMontant(){

        //récupérer le repository
        $repository = $this->getDoctrine()->getRepository(Pot::class);
        //je lis la BDD
        $pot = $repository->findAll();


        $persons = $this->getPerson();
        $tot = 0;
        dump($persons);
        foreach($persons as $p){
            $tot = $tot + $p['montant_paye'];
        }

        return $tot;

    }

    /*public function verifMontantTot(){
        $montantPersonnes = $this->calculMontant();
        $pottt = $this->lastPotCreated();
        $montantPot = $pottt[0]['montant_tot'];

        if($montantPersonnes > $montantPot){
            $montantFin = $montantPersonnes;
        } else {
            $montantFin = $montantPot;
        }

        $entityManager = $this->getDoctrine()->getManager();
        $pot = $entityManager->getRepository(Pot::class)->find($pottt[0]['id']);
        $pot->setMontantTot($montantFin);
        $entityManager->flush();


        return $montantFin;
    }*/

    public function montantMoy(){
        $montantPersonnes = $this->calculMontant();
        $pottt = $this->lastPotCreated();
        $persons = $this->getPerson();
        $montantPot = $pottt[0]['montant_tot'];

        $nbPersonnes = 0;
        if($montantPersonnes > $montantPot){
            $montantFin = $montantPersonnes;
        } else {
            $montantFin = $montantPot;
        }


        foreach($persons as $p){
            $nbPersonnes ++;
        }

        $montantMoy = $montantFin / $nbPersonnes;
        $montantMoy = round( $montantMoy, 2);

        $entityManager = $this->getDoctrine()->getManager();
        $pot = $entityManager->getRepository(Pot::class)->find($pottt[0]['id']);
        $pot->setMontantTot($montantFin);
        $pot->setMontantMoy($montantMoy);
        $entityManager->flush();


        return $montantMoy;
    }

 /*   public function setMontRestPerson(float $montMoy){


    }*/
}
