<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Person;
use App\Entity\Pot;
use App\Form\PersonType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    /**
     * @Route("/person/${id}", name="person")
     */
    public function personne($id, Request $request)
    {
        $personne = new Person();

        $form = $this->createForm(PersonType::class, $personne);

        $repository=$this->getDoctrine()->getRepository(Pot::class);
        $pot = $repository->find($id);
        $personne->setIdPot($pot);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entitymanag = $this->getDoctrine()->getManager();

            $entitymanag->persist($personne);

            $entitymanag->flush();

            return $this->redirectToRoute('pot');
        }

        return $this->render('/person/index.html.twig',[
            "formulaire"=>$form->createView()
        ]);
    }
}

