<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Pot;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="presentation")
     */
    public function index(): Response
    {

        //récupérer le repository
        $repository = $this->getDoctrine()->getRepository(Pot::class)
            ->findBy([],['name'=> 'desc']);
        $repodeux =$this ->getDoctrine()->getRepository(Person::class)
            ->findBy([],['idPot'=>'desc']);
        //je lis la BDD
        $personne =$repodeux;
        $pot = $repository;
     //   $personne =$pot->getIdPot();
        $nameLastPot = $this->lastPotCreated();

        return $this->render('/accueil/index.html.twig', [
            'controller_name' => 'Arthur',
            "pot" => $pot ,
            'personne'=>$personne,
            'lastPot' => $nameLastPot,

        ]);

    }

    public function lastPotCreated(){
        $em=$this->getDoctrine()->getManager();

        $query='SELECT name, id FROM pot ORDER BY id DESC LIMIT 0, 1';

        $res = $em->getConnection()->prepare($query);

        $res->execute();

        $reponse = $res->fetchAll();
        return $reponse;
    }
}
